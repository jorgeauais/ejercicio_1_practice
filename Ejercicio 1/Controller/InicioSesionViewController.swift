//
//  InicioSesionViewController.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 09/06/2021.
//

import UIKit


class InicioSesionController: UIViewController {
    
    var check = Autorizar()
    
    @IBOutlet weak var nombreUsuario: UITextField!
    @IBOutlet weak var contraseñaUsuario: UITextField!
    @IBOutlet weak var errorTexto: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func irRegistro(_ sender: UIButton) {
        errorTexto.text = ""
        performSegue(withIdentifier: "Registro", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Registro" {
            let datos = segue.destination as! RegistroSesionViewController
            datos.registro = self.check
        }
    }
    
    @IBAction func botonListo(_ sender: UIButton) {
        errorTexto.text = ""
        if check.validar(nombreUsuario.text ?? "", contraseñaUsuario.text ?? "") {
            
            performSegue(withIdentifier: "Principal", sender: self)
            
        } else {
            errorTexto.text = "Su usuario no existe"
        }
    }
    
    
}
