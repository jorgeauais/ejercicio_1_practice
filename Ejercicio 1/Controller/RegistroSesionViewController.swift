//
//  RegistroSesion.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 09/06/2021.
//

import UIKit

class RegistroSesionViewController: UIViewController {
    
    var registro = Autorizar()

    @IBOutlet weak var nombreUsuario: UITextField!
    @IBOutlet weak var correo: UITextField!
    @IBOutlet weak var contraseña: UITextField!
    @IBOutlet weak var error: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func registrar(_ sender: UIButton) {
        if registro.checar(nombreUsuario.text ?? "", correo.text ?? "") {
            error.text = ""
            if  registro.agregarUsuario(nombreUsuario.text ?? "", correo.text ?? "",  contraseña.text ?? "") {
                dismiss(animated: true, completion: nil)
            } else {
                error.text = "Datos invalidos"
            }
        } else {
            error.text = "El nombre o correo ya esta asignado a un usario existente"
        }
    }
}
