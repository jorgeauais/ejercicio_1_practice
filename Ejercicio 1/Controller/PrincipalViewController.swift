//
//  ViewController.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 08/06/2021.
//

import UIKit

class PrincipalViewController: UIViewController {
    
    var lista = ListaPeliculas()

    @IBOutlet weak var dots: UIPageControl!
    @IBOutlet weak var nombreTop: UILabel!
    @IBOutlet weak var nombreMiddle: UILabel!
    @IBOutlet weak var descripcionMiddle: UILabel!
    @IBOutlet weak var descripcionBottom: UILabel!
    @IBOutlet weak var nombreBottom: UILabel!
    @IBOutlet weak var imagenTop: UIButton!
    @IBOutlet weak var imagenMiddle: UIButton!
    @IBOutlet weak var imagenBottom: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func cerrarSesion(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cambioPelicula(_ sender: UIButton) {
        lista.cambiarPelicula(sender.accessibilityIdentifier!)
        
        nombreTop.text = lista.peliculaActualTop.nombre
        imagenTop.setImage(lista.peliculaActualTop.portada, for: .normal)
        
        nombreMiddle.text = lista.peliculaActualMid.nombre
        imagenMiddle.setImage(lista.peliculaActualMid.portada, for: .normal)
        descripcionMiddle.text = lista.peliculaActualMid.descripcion
        
        nombreBottom.text = lista.peliculaActualBot.nombre
        imagenBottom.setImage(lista.peliculaActualBot.portada, for: .normal)
        descripcionBottom.text = lista.peliculaActualBot.descripcion
    }
    
    @IBAction func elegirPelicula(_ sender: UIButton) {
        lista.peliculaElegida(sender.accessibilityIdentifier!)
        performSegue(withIdentifier: "PeliculaElegida", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pelicula = segue.destination as! PeliculaElegidaViewController
        pelicula.portada.image = lista.pelicula.portada
        pelicula.nombre.text = lista.pelicula.nombre
        pelicula.descripcion.text = lista.pelicula.descripcion
        pelicula.urlString = lista.pelicula.url
    }
}

