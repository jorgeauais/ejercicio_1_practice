//
//  PeliculaElegidaViewController.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 10/06/2021.
//

import UIKit
import SafariServices

class PeliculaElegidaViewController: UIViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var portada: UIImageView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var descripcion: UILabel!
    
    var urlString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func regresar(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func verTrailer(_ sender: UIButton) {
        if let url = URL(string: urlString) {
            let video = SFSafariViewController(url: url)
            video.delegate = self
            present(video, animated: true)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        dismiss(animated: true, completion: nil)
    }
}
