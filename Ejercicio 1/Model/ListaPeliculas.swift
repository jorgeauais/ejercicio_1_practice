//
//  ListaPeliculas.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 10/06/2021.
//

import UIKit

class ListaPeliculas {
    var listaPeliculasTop: [Pelicula] = []
    var listaPeliculasMid: [Pelicula] = []
    var listaPeliculasBot: [Pelicula] = []
    var numeroPelicula: [Int] = [0]
    var peliculaActualTop = Pelicula("imagen.jpeg", "", "", "")
    var peliculaActualMid = Pelicula("imagen.jpeg", "", "", "")
    var peliculaActualBot = Pelicula("imagen.jpeg", "", "", "")
    var pelicula = Pelicula("imagen.jpeg", "", "", "")
    
    init() {
        //En este inicializador se llamaria a los datos de TMBD
        var i = 0
        while i < 5 {
            i += 1
            listaPeliculasTop.append(Pelicula("imagen.jpeg", "", "", ""))
            listaPeliculasMid.append(Pelicula("imagen.jpeg", "", "", ""))
            listaPeliculasBot.append(Pelicula("imagen.jpeg", "", "", ""))
        }
        numeroPelicula[0] = 0
        numeroPelicula.append(0)
        numeroPelicula.append(0)
    }
    
    func cambiarPelicula(_ nombre: String) {
        switch nombre {
        case "IT":
            if numeroPelicula[0] > 0 {
                numeroPelicula[0] -= 1
                peliculaActualTop = listaPeliculasTop[numeroPelicula[0]]
            }
        case "DT":
            if numeroPelicula[0] < 5 {
                numeroPelicula[0] += 1
                peliculaActualTop = listaPeliculasTop[numeroPelicula[0]]
            }
        case "IM":
            if numeroPelicula[1] > 0 {
                numeroPelicula[1] -= 1
                peliculaActualMid = listaPeliculasMid[numeroPelicula[1]]
            }
        case "DM":
            if numeroPelicula[1] < 5 {
                numeroPelicula[1] += 1
                peliculaActualMid = listaPeliculasMid[numeroPelicula[1]]
            }
        case "IB":
            if numeroPelicula[2] > 0 {
                numeroPelicula[2] -= 1
                peliculaActualMid = listaPeliculasMid[numeroPelicula[2]]
            }
        case "DB":
            if numeroPelicula[2] < 5 {
                numeroPelicula[2] += 1
                peliculaActualMid = listaPeliculasMid[numeroPelicula[2]]
            }
        default:
            pelicula = Pelicula("imagen.jpeg", "", "", "")
        }
    }
    
    func peliculaElegida(_ nombre: String) {
        switch nombre {
        case "imagenTop":
            pelicula = peliculaActualTop
        case "imagenMid":
            pelicula = peliculaActualMid
        case "imagenBot":
            pelicula = peliculaActualBot
        default:
            pelicula = Pelicula("imagen.jpeg", "", "", "")
        }
    }
}
