
class Usuario {
    var nombre: String = ""
    var correo: String = ""
    var contraseña: String = ""
    
    init (_ correo: String, _ contraseña: String, _ nombre: String) {
        self.correo = correo
        self.contraseña = contraseña
        self.nombre = nombre
    }
}
