//
//  Peliculas.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 10/06/2021.
//

import UIKit

class Pelicula {
    var portada: UIImage
    var nombre: String
    var descripcion: String
    var url: String
    
    init (_ portada: String, _ nombre: String, _ descripcion: String, _ url: String) {
        self.portada = UIImage(named: portada)!
        self.nombre = nombre
        self.descripcion = descripcion
        self.url = url
    }
}
