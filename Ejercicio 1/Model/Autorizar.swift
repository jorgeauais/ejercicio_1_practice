//
//  Mind.swift
//  Ejercicio 1
//
//  Created by Fede Aguilar on 09/06/2021.
//

import Foundation

class Autorizar {
    
    var usuarios: [Usuario] = []
    var nuevoUsuario = Usuario("", "", "")
    
    func validar(_ nombre: String, _ contraseña: String) -> Bool {
        var listo = false
        
        for i in 0..<usuarios.count {
            if usuarios[i].nombre == nombre && usuarios[i].contraseña == contraseña {
                listo = true
            }
        }
        
        return listo
    }
    
    func checar(_ nombre: String, _ correo: String) -> Bool {
        var listo = true
        
        for i in 0..<usuarios.count {
            if usuarios[i].nombre == nombre && usuarios[i].correo == correo {
                listo = false
            }
        }
        
        return listo
    }
    
    func agregarUsuario(_ nombre: String, _ correo: String, _ contraseña: String) -> Bool {
        var listo = false
        
        if !(nombre == "" || correo == "" || contraseña == "") {
            
            let patron = "[\\d\\w._-]+@[\\w]+.[\\w]{2,}"
            var check: [String]
            do {
                let regex = try NSRegularExpression(pattern: patron)
                let resultado = regex.matches(in: correo, range: NSRange(correo.startIndex..., in: correo))
                
                check = resultado.map {
                    String(correo[Range($0.range, in: correo)!])
                }
                nuevoUsuario = Usuario(correo, contraseña, nombre)
                usuarios.append(nuevoUsuario)
            } catch {
                check = []
            }
            listo = (check != [])
        }
        
        return listo
    }
}
